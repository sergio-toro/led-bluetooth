/**
 * Comandos:
 * 
 * AT+NAME?         Devuelve el nombre del dispositivo
 * AT+NAME=NEW_NAME Configura el nombre del dispositivo
 * 
 * AT+PSWD?         Devuelve la contraseña configurada actualmente
 * AT+PSWD=NEW_PASS Configura la contraseña del dispositivo
 * 
 * AT+UART?         Devuelve la configuración de comunicación con la placa arduino
 * AT+CLASS?        Default: 1f00
 *
 * AT+ADDR?         Get module Bluetooth address
 * 
 * AT+ROLE?
 * 0 => Modo esclavo (Otro dispositivo se conecta al HC05)
 * 1 => Modo master  (HC05 se conecta a otro dispositivo)
 */
#include <SoftwareSerial.h>

#define RxD 11 //
#define TxD 10
//#define RST 5
#define MODE 4   // LOW=> modo cominicación, HIGH=> modo de configuracion

#define LED 13

SoftwareSerial BTSerial(TxD, RxD);
String readedData;

String BTSerialData;
String serialData;

void setup()
{
    //pinMode(RST, OUTPUT);
    //digitalWrite(RST, LOW);
    //digitalWrite(RST, HIGH);

    // BT Mode comunicaciones
    pinMode(MODE, OUTPUT);
    pinMode(LED,  OUTPUT);
    
    digitalWrite(MODE, LOW); // Configuracion
    //digitalWrite(MODE, LOW); // Comunicaciones
    
    // Configure BT
    BTSerial.begin(38400);
    BTSerial.flush();    
    delay(500);
    
    // Configure USB Serial
    Serial.begin(9600);

    debug("LED program started.");
}

void loop()
{
    if (BTSerial.available()) {
        //Serial.write(BTSerial.read());
        readedData = getBTData();

        //writeSerial(BTSerialData);
        //debug(BTSerialData);

        if (readedData != "") {
            processBTData(readedData);
        }
    }

    if (Serial.available()) {
        serialData = getSerialData();
        
        if (serialData != "") {
            processSerialData(serialData);
        }
    }
    delay(1);
}

void processBTData(String data) {
    String uData = data;
    uData.toUpperCase();

    // Procesar comando de Bluetooth
    if (uData.startsWith("BT")) {
        processBTCommand(uData);
    }
    // Procesar comando de LED
    else if (uData.startsWith("LED")) {
        processLEDCommand(uData);
    }
    else {
        debug("Comando no reconocido");
    }
}

void processSerialData(String data) {
    String uData = data;
    uData.toUpperCase();

    // Procesar comando de Bluetooth
    if (uData.startsWith("BT")) {
        processBTCommand(uData);
    }
    // Procesar comando de LED
    else if (uData.startsWith("LED")) {
        processLEDCommand(uData);
    }
    // Pasar comando al BT-HC05
    else if (uData.startsWith("AT")){
        writeBT(data);
    }
    else {
        debug("Comando no reconocido");
    }
}

void processBTCommand(String data) {
    data = data.substring(2);
    data.trim();

    if (data=="CONF") {
        digitalWrite(MODE, HIGH); // Configuracion
        debug("BT modo configuracion");
    }
    else if (data=="COM") {
        digitalWrite(MODE, LOW); // Comunicaciones
        debug("BT modo comunicaciones");
    }
    else {
        debug("BT comando no reconocido");
    }
}

void processLEDCommand(String data) {
    data = data.substring(3);
    data.trim();

    if (data=="ON") {
        digitalWrite(LED, HIGH);
        debug("LED encendido");
    }
    else if (data=="OFF") {
        digitalWrite(LED, LOW);
        debug("LED apagado");
    }
    else {
        debug("LED comando no reconocido");
    }
}

void debug(String message) {
    Serial.print("[DEBUG] ");
    Serial.println(message);
}

void writeBT(String message) {
    int length = message.length();
    byte bytes[length];

    message.getBytes(bytes, length);

    for (int i = 0; i < length; ++i) {
        BTSerial.write(bytes[i]);
    }
}

void writeSerial(String message) {
    int length = message.length();
    byte bytes[length];

    message.getBytes(bytes, length);

    for (int i = 0; i < length; ++i) {
        Serial.write(bytes[i]);
    }
}

String getSerialData() {
    String content = "";
    char character;

    while(Serial.available()) {
        character = Serial.read();
        content.concat(character);

        delay(5);
    }
    
    return content;
}

String getBTData() {
    String content = "";
    char character;

    while(BTSerial.available()) {
        character = BTSerial.read();
        content.concat(character);

        delay(10);
    }
    
    return content;
}