(function() {
    "use strict";


    var DeviceService = function($rootScope, $timeout, $ionicLoading, $ionicPopup, Storage, Bluetooth) {
        var self = this;

        this.$rootScope    = $rootScope;
        this.$timeout      = $timeout;
        this.$ionicLoading = $ionicLoading;
        this.$ionicPopup   = $ionicPopup;
        this.Storage       = Storage;
        this.Bluetooth     = Bluetooth;
        this.connectErrors = 0;
        this.data          = {
            device: null
        };


        self.loadData();

        // Handle connection to device
        self.$rootScope.$watch(function() { return self.data.device; },
            function onChanges(device, previousDevice) {
                if (device && device.id) {
                    if (!device.connected) {
                        self.connect(device);
                    }
                }
                else if (previousDevice && previousDevice.connected) {
                    self.disconnect(previousDevice);
                }

            }
        );
    };


    DeviceService.prototype.connect = function(device) {
        var self = this;

        self.$ionicLoading.show({
            template: "Conectando al dispositivo '"+device.name+"'..."
        });
        self.Bluetooth.connect(
            device.id,
            function onSuccess() {
                // Set device as connected
                device.connected   = true;
                self.connectErrors = 0;
                self.$ionicLoading.hide();
                self.$rootScope.$apply();
            },
            function onError(){
                // Try to connect again (max 3 times)
                if (self.connectErrors <= 2) {
                    self.$timeout(function(){
                        self.connect(device);
                    }, 1000);
                    self.connectErrors++;
                }
                else {
                    // Clear device, max errors reached
                    self.clearDevice();
                    self.connectErrors = 0;
                    self.$ionicLoading.hide();
                    self.$ionicPopup.alert({
                        title: 'Error',
                        template: 'No se puede conectar con el dispositivo',
                    });
                }

                self.$rootScope.$apply();
            }
        );
    };

    DeviceService.prototype.disconnect = function(device) {
        var self = this;

        self.Bluetooth.disconnect(
            function onSuccess() {
                // Disconnected successfully
            },
            function onError() {
                self.$ionicPopup.alert({
                    title:    'Error',
                    template: "No se ha podido cerrar la conexión con el dispositivo '"+device.name+"'.",
                });
                self.$rootScope.$apply();
            }
        );
    };


    DeviceService.prototype.loadData = function() {
        var self = this;

        if (self.Storage.getItem('device.id')) {
            self.data.device = {
                id:      self.Storage.getItem('device.id'),
                name:    self.Storage.getItem('device.name'),
                address: self.Storage.getItem('device.address')
            };
        }
    };

    // Gets public service
    DeviceService.prototype.clearDevice = function() {
        var self = this;

        self.Storage.removeItem('device.id');
        self.Storage.removeItem('device.name');
        self.Storage.removeItem('device.address');

        self.data.device = null;
    };
    DeviceService.prototype.getPublicService = function() {
        var self = this;

        return {
            data: self.data,
            get: function() {
                return self.data.device;
            },
            set: function(device) {
                self.data.device = {
                    id:      device.id,
                    name:    device.name,
                    address: device.address
                };

                self.Storage.setItem('device.id',      self.data.device.id);
                self.Storage.setItem('device.name',    self.data.device.name);
                self.Storage.setItem('device.address', self.data.device.address);
            },
            clear: function() {
                self.clearDevice();
            },
            refresh: function() {
                self.loadData();
            }
        };
    };

    angular.module('LED.services.device', [])
    .factory('Device', [
        '$rootScope',
        '$timeout',
        '$ionicLoading',
        '$ionicPopup',
        'Storage',
        'Bluetooth',
        function($rootScope, $timeout, $ionicLoading, $ionicPopup, Storage, Bluetooth) {
            var Device = new DeviceService($rootScope, $timeout, $ionicLoading, $ionicPopup, Storage, Bluetooth);

            return Device.getPublicService();
        }
    ]);
})();


