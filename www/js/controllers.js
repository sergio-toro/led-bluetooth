(function() {
    "use strict";

    //////////////////////////////////////////////////////////////////////////////////////
    // Config Controller
    //////////////////////////////////////////////////////////////////////////////////////

    var ConfigCtrl = function($scope, $ionicPopup, Bluetooth, Device) {
        var self    = this;
        $scope.ctrl = self;

        self.$scope      = $scope;
        self.$ionicPopup = $ionicPopup;
        self.Device      = Device;

        Bluetooth.list(
            function onSuccess(devices) {
                $scope.$apply(function() {
                    var storedDevice = self.Device.get();
                    self.devices     = devices;

                    if (storedDevice && storedDevice.id) {
                        self.selectDevice(storedDevice.id);
                    }
                });
            },
            function onError(data){
                $scope.$apply(function() {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'No se pueden obtener los dispositivos Bluetooth.',
                    });
                });
            }
        );

        // Watch associated device
        $scope.$watch(function() { return Device.data.device; },
            function onChanges(device) {
                if (device && device.id) {
                    self.selectDevice(device.id);
                }
                else {
                    self.unselectDevices();
                }
            }
        );
    };

    ConfigCtrl.prototype.$inject = [
        '$scope',
        '$ionicPopup',
        'Bluetooth',
        'Device'
    ];

    ConfigCtrl.prototype.deviceChanged = function(device) {
        var self = this;

        self.Device.set(device);
    };

    ConfigCtrl.prototype.clearDevice = function() {
        var self = this;

        var confirmPopup = self.$ionicPopup.confirm({
            title: 'ELiminar configuración',
            template: '¿Estás seguro de que deseas eliminar la asociación de dispositivos?',
            okText: 'Aceptar',
            cancelText: 'Cancelar'
        });
        confirmPopup.then(function(result) {
            if (result) {
                self.Device.clear();

                self.unselectDevices();
            }
        });
    };

    ConfigCtrl.prototype.unselectDevices = function() {
        var self = this;

        angular.forEach(self.devices, function(device) {
            device.selected = null;
        });
    };

    ConfigCtrl.prototype.selectDevice = function(id_device) {
        var self = this;

        angular.forEach(self.devices, function(device) {
            if (id_device == device.id && !device.selected) {
                device.selected = 1;
            }
        });
    };

    //////////////////////////////////////////////////////////////////////////////////////
    // Dashboard Controller
    //////////////////////////////////////////////////////////////////////////////////////

    var DashCtrl = function($scope, $ionicLoading, Bluetooth, Device) {
        var self    = this;
        $scope.ctrl = self;

        this.$scope        = $scope;
        this.$ionicLoading = $ionicLoading;
        this.Bluetooth     = Bluetooth;

        this.device = null;

        // Watch connected device
        $scope.$watch(function() { return Device.data.device; },
            function onChanges(device) {
                self.device = device;
            }
        );
    };

    DashCtrl.prototype.$inject = [
        '$scope',
        '$ionicLoading',
        'Bluetooth',
        'Device',
    ];

    DashCtrl.prototype.toggleLed = function(device) {
        var self = this;

        self.$ionicLoading.show({
            template: (device.ledEnabled ? 'Encendiendo LED...' : 'Apagando LED...')
        });

        // Send message
        self.Bluetooth.write(
            device.ledEnabled ? 'LED ON' : 'LED OFF',
            function onSuccess(devices) {
                self.$scope.$apply(function() {
                    self.$ionicLoading.hide();
                });
            },
            function onError(data){
                console.log("Error al cambiar el estado del led");
            }
        );
    };

    angular.module('LED.controllers', [])
    .controller('DashCtrl', DashCtrl)
    .controller('ConfigCtrl', ConfigCtrl);
})();