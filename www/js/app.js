(function(){
    "use strict";

    angular.module('LED', ['ionic', 'ngCordova', 'LED.controllers', 'LED.services'])

    .run([
        '$ionicPlatform',
        '$cordovaStatusbar',
        function($ionicPlatform, $cordovaStatusbar) {
            $ionicPlatform.ready(function() {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if(window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if(window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    //$cordovaStatusbar.overlaysWebView(true);
                    // styles: Default : 0, LightContent: 1, BlackTranslucent: 2, BlackOpaque: 3
                    //$cordovaStatusbar.style(1);
                    // supported names: black, darkGray, lightGray, white, gray, red, green,
                    // blue, cyan, yellow, magenta, orange, purple, brown
                    //$cordovaStatusbar.styleColor('white');
                    $cordovaStatusbar.show();
                    //$cordovaStatusbar.hide();
                }
            });
        }
    ])
    .config(function($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            // setup an abstract state for the tabs directive
            .state('tab', {
                url: "/tab",
                abstract: true,
                templateUrl: "templates/tabs.html",
            })

            // Each tab has its own nav history stack:

            .state('tab.dash', {
                url: '/dashboard',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/tab-dash.html',
                        controller: 'DashCtrl'
                    }
                }
            })

            .state('tab.config', {
                url: '/config',
                views: {
                    'tab-config': {
                        templateUrl: 'templates/tab-config.html',
                        controller: 'ConfigCtrl',
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/dashboard');

    })

    .run([
        '$rootScope',
        '$timeout',
        '$ionicLoading',
        '$ionicPopup',
        'Device',
        'Bluetooth',
        function($rootScope, $timeout, $ionicLoading, $ionicPopup, Device, Bluetooth) {
            var
                checkBTEnabled = function() {
                    Bluetooth.isEnabled(function onSuccess() {
                        $rootScope.$apply(function(){
                            $ionicLoading.hide();
                            $timeout(checkBTEnabled, 1000 * 30);
                        });
                    }, function onError() {
                        $rootScope.$apply(function(){
                            $ionicLoading.show({
                                template: 'Bluetooth desactivado'
                            });
                            $timeout(checkBTEnabled, 1000);
                        });
                    });
                }
            ;

            // Only in mobile device
            if (window.cordova) {
                checkBTEnabled();
            }
        }
    ]);

})();