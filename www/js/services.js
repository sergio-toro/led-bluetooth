(function() {
    "use strict";

    angular.module('LED.services', [
        'LED.services.device'
    ])

    /**
     * BTSerial wrapper
     */
    .factory('Bluetooth', function() {

        return {
            isEnabled: function(successFn, errorFn) {
                if (!window.bluetoothSerial) {
                    // Timeout for async execution
                    setTimeout(errorFn, 1);
                    return;
                }
                window.bluetoothSerial.isEnabled(successFn, errorFn);
            },
            list: function(successFn, errorFn) {
                if (!window.bluetoothSerial) {
                    // Timeout for async execution
                    setTimeout(errorFn, 1);
                    return;
                }
                window.bluetoothSerial.list(successFn, errorFn);
            },
            isConnected: function(successFn, errorFn) {
                if (!window.bluetoothSerial) {
                    // Timeout for async execution
                    setTimeout(errorFn, 1);
                    return;
                }
                window.bluetoothSerial.isConnected(successFn, errorFn);
            },
            connect: function(identifier, successFn, errorFn) {
                var self = this;
                if (!window.bluetoothSerial) {
                    // Timeout for async execution
                    setTimeout(errorFn, 1);
                    return;
                }
                self.isConnected(
                    function onSuccess() {
                        // Device already connected, try to disconnect first
                        self.disconnect(
                            function onSuccess(){
                                // disconnected succefully, try to connect
                                window.bluetoothSerial.connect(identifier, successFn, errorFn);
                            },
                            function onError() {
                                // Error disconnecting, call error
                                errorFn("Error al cerrar conexión antigua.");
                            }
                        );
                    },
                    function onError() {
                        // device not connected it's safe to connect
                        window.bluetoothSerial.connect(identifier, successFn, errorFn);
                    }
                );
            },
            disconnect: function(successFn, errorFn) {
                if (!window.bluetoothSerial) {
                    // Timeout for async execution
                    setTimeout(errorFn, 1);
                    return;
                }
                window.bluetoothSerial.disconnect(successFn, errorFn);
            },
            write: function(message, successFn, errorFn) {
                if (!window.bluetoothSerial) {
                    // Timeout for async execution
                    setTimeout(errorFn, 1);
                    return;
                }
                window.bluetoothSerial.write(message, successFn, errorFn);
            },
        };
    })


    /**
     * BTSerial wrapper
     */
    .factory('Storage', function() {
        return {
            getItem: function(name) {
                return window.localStorage ? window.localStorage.getItem(name)        : null;
            },
            setItem: function(name, value) {
                return window.localStorage ? window.localStorage.setItem(name, value) : null;
            },
            removeItem: function(name) {
                return window.localStorage ? window.localStorage.removeItem(name)     : null;
            },
            clear: function() {
                return window.localStorage ? window.localStorage.clear()              : null;
            }
        };
    });
})();


